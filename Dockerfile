## TODO + add version variables
##      add sdk,doc etc sources   
##      add properties file overrides
##	https://registry.hub.docker.com/u/dmean/liferay-tomcat/dockerfile/
##	https://registry.hub.docker.com/u/snasello/liferay-6.2/dockerfile/
##	https://registry.hub.docker.com/search?q=liferay
##	add install java && portal from locally added zips, and choice for dbms and web server

FROM ubuntu:trusty


MAINTAINER Aleksey Oblozhko <oblozhko@gmail.com>

ENV DEBIAN_FRONTEND noninteractive 
ENV LIFERAY_VER 6.2-ce-ga2
ENV LIFERAY_FOLDER 6.2.1%20GA2
ENV LIFERAY_BUILD 20140319114139101

# Install java && stuff
RUN \
  apt-get update && \
  apt-get install -y unzip curl software-properties-common && \
  add-apt-repository ppa:webupd8team/java && \
  apt-get update && \
  echo debconf shared/accepted-oracle-license-v1-1 select true | debconf-set-selections && \
  apt-get install -y oracle-java7-installer oracle-java7-set-default 

##libtcnative-1 mysql-client 

RUN \
  curl -O -s -k -L -C - http://downloads.sourceforge.net/project/lportal/Liferay%20Portal/$LIFERAY_FOLDER/liferay-portal-tomcat-$LIFERAY_VER-$LIFERAY_BUILD.zip && \
  unzip liferay-portal-tomcat-$LIFERAY_VER-$LIFERAY_BUILD.zip -d /opt && \
  rm liferay-portal-tomcat-$LIFERAY_VER-$LIFERAY_BUILD.zip && \
  ln -sf /opt/liferay-portal-$LIFERAY_BUILD/ /opt/liferay

## install APR
##RUN export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/liferay-portal/tomcat-7.0.42/lib

# Clean up APT when done.
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Volumes
#VOLUME ["/var/liferay", "/opt/liferay-portal/deploy", "/opt/liferay-portal/data"]

# Configure liferay
#ENV JAVA_OPTS '-Dexternal-properties=/var/liferay/portal-ext.properties



EXPOSE 8080
ENTRYPOINT ["/opt/liferay/tomcat-7.0.42/bin/startup.sh"]
